import random
import tqdm
import numpy as np

def generate(n):
    nums = []
    while len(nums) < n:
        new_num = random.randint(1,25)    
        if new_num not in nums:
            nums.append(new_num)
    nums.sort()
    return nums

def evaluate(x, y):
    accuracy = 0
    for i in x:
        if i in y:
            accuracy += 1
    return accuracy

def calculate_prize(score, n):
    prize =  {
        11 : 5,
        12 : 10,
        13 : 25,
        14 : 1852.84,
        15 : 1495244.98
    }

    return prize[score] * n

def estimate(n_games):
    prize =  {
        11 : 0,
        12 : 0,
        13 : 0,
        14 : 0,
        15 : 0,
        0  : 0
    }

    for _ in range(0, n_games):
        player = generate(15)
        board = generate(15)
        score = evaluate(player, board)
        if score > 10:
            prize[score] += 1
        else:
            prize[0] += 1

    # print("statistic for %s games\n" % n_games)
    # for key in prize: #total
    #     print("%s - %s" % (key, prize[key]))

    # for key in prize: #percent
    #     print("%s - %0.1f%%" % (key, (prize[key]/n_games)*100))

    total_outcome = 0
    games_price = 2.5 * n_games
    for key in prize:
        if key != 0:
            total_outcome += calculate_prize(key, prize[key])
    total_outcome -= games_price
    # print("total balance: %s" % total_outcome)
    return total_outcome

def main():
    n_times = 100000
    won = []
    lost = []
    for i in tqdm.tqdm(range(0, n_times)):
        outcome = estimate(104)
        if outcome > 0:
            won.append(outcome)
        else:
            lost.append(outcome)
    print("won:", won)
    print("lost:", lost)

    print("won: %0.2f" % ((len(won) / n_times)*100))
    print("lost: %0.2f" % ((len(lost) / n_times)*100))
    total = won + lost
    won = np.asarray(won, dtype=np.dtype("float32"))
    lost = np.asarray(lost, dtype=np.dtype("float32"))
    total = np.asarray(total, dtype=np.dtype("float32"))
    print("median win prize %0.2f" % np.median(won) )
    print("median lost prize %0.2f" % np.median(lost) )
    print("median total prize %0.2f" % np.median(total) )

main()