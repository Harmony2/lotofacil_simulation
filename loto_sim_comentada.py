import random
import numpy as np

# função pra gerar cartelas
def generate(n):
    nums = []
    while len(nums) < n: # enquanto a lista tiver menos que n items
        new_num = random.randint(1,25) # sorteia um item de 1 a 25
        if new_num not in nums: # se esse número não estiver na lista
            nums.append(new_num) # adiciona na lista
    nums.sort() # ordena lista (crescente)
    return nums


# função pra avaliar número de acertos
def evaluate(x, y): # X = lista de números do jogador & Y = lista de números sorteados
    accuracy = 0 # incidência de matches
    for i in x: # pra cada número i na lista do jogador
        if i in y: # se i estiver na lista dos números sorteados:
            accuracy += 1 # acuracia = acuracia + 1
    return accuracy


# função pra calcular o prêmio baseado na quantidade de acertos
def calculate_prize(score, n): # z
    prize =  { # tabela de prêmios. {acertos : premiação}
        11 : 5,
        12 : 10,
        13 : 25,
        14 : 1852.84,
        15 : 1495244.98
    }

    return prize[score] * n # retorna o prêmio * quantidade de vezes que ele apareceu na simulação 

def estimate(n_games): #estima as probabilidades de acerto em n jogos
    prize =  {
        11 : 0,
        12 : 0,
        13 : 0,
        14 : 0,
        15 : 0,
        0  : 0
    }

    for _ in range(0, n_games):
        player = generate(15) #gera a tabela do jogador
        board = generate(15) #gera a tabela sorteada do jogo
        score = evaluate(player, board) #faz a avaliação de acerto entre os dois
        if score >= 11: #se atingir pelo menos 11 pontos
            prize[score] += 1 #adiciona 1 pro dicionário de prize na chave 11
        else: #caso contrário 
            prize[0] += 1 #adiciona 1 pro dicionário de prize na chave 0

    # # print("statistic for %s games\n" % n_games)
    # # for key in prize: #total
    # #     print("%s - %s" % (key, prize[key]))

    # # for key in prize: #percent
    # #     print("%s - %0.1f%%" % (key, (prize[key]/n_games)*100))

    total_outcome = 0 #balanço monetário final
    games_price = 2.5 * n_games #preço dos jogos
    for key in prize:
        if key != 0:
            total_outcome += calculate_prize(key, prize[key]) #balanço monetário final += calculo do prêmio
    total_outcome -= games_price
    # print("total balance: %s" % total_outcome)
    return total_outcome #retorna balanço monetário final

def main():
    n_times = 100
    won = []
    lost = []
    for _ in (range(0, n_times)):
        outcome = estimate(104) #calcula o resultado final de 104 jogos
        if outcome > 0: #caso o resultado final seja positivo
            won.append(outcome) #adiciona à lista won
        else: #caso contrário
            lost.append(outcome) #adiciona à lista lost
    print("won:", won)
    print("lost:", lost)

    print("won: %d %0.2f%%" % (len(won), (len(won) / n_times)*100))
    print("lost: %d %0.2f%%" % (len(lost), (len(lost) / n_times)*100))
    total = won + lost
    won = np.asarray(won, dtype=np.dtype("float32"))
    lost = np.asarray(lost, dtype=np.dtype("float32"))
    total = np.asarray(total, dtype=np.dtype("float32"))

    if len(won) > 0 : print("median win prize %0.2f" % np.median(won)) #média dos resultados monetários apenas das vitórias

    if len(lost) > 0 : print("median lost prize %0.2f" % np.median(lost)) #média dos resulados monetários apenas das derroas

    print("median total prize %0.2f" % np.median(total)) #média total dos resultados

main()